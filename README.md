# Projet Sélection Technique CDA

Dans un dépôt Gitlab/Github dont tu nous donneras le lien :

Créer une API Rest pour une pseudo-application de forum correspondante au diagramme d'entité ci-dessous

![diagramme](./selection-uml.jpg)

* Rendre le CRUD disponibles via une API REST sur ces entités
* Il n'y a pas de langage imposé, mais il y a une forte probabilité que les cours du CDA soient en Java ou en Node.js avec Typescript
* Les DAO/Repository devront être fait "à la main", pourquoi pas utiliser un framework ou micro-framework pour le routing mais pour la partie data faire sans ORM (l'idée étant de voir les requêtes bdd, préférence pour du SQL, mais pas obligatoire)
* Pas nécessaire de faire le Front de l'application, l'api suffira


